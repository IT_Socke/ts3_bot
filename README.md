# Run a Teamspeak Bot on an raspberry pi with docker

## Setup ssh connection for your local machine
For a comfortable deployment it is recommended to establish a connection to the raspberry pi using an [ssh key](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) without password. If not already done do that and in the end copy the ssh public key file to your pi:
```
cat <path_to_public_key> | ssh <my_pi_ssh_alias> 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'
```

Create also an [ssh alias](https://www.ostechnix.com/how-to-create-ssh-alias-in-linux/) for your pi connection. You can copy the content from file `/ssh/config` in your local `config` file an replace the parameters for `hostname` and `port`. For a later automatic deploment of gitlab to your pi you should create a dyndns entry, set it in your router configuration, create a ssh port forwarding rule and set this parameters in your `config` file.

## Install docker on your pi
Followed by [this](sudo apt-get update && sudo apt-get upgra) instructions enter these commands on pi to install docker:
```
sudo apt-get update && sudo apt-get upgrade -y
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker pi
```
Now `logout` and login to check docker installation by command `docker version` without `sudo` command. You also need activate docker swarm by typing:
```
docker swarm init
```

## Check architecture
You can find out which architecure your machine uses by using the `arch` command. if it is armv7l (on every raspberry pi 3) you can leave the `docker-compose.yml` file like this. Otherwise another [image](https://gitlab.com/IT_Socke/ts3_bot/container_registry/) must be registered.

## Manually deployment from local maschine
You can trigger the deployment by typing:
```
export TS3_IP_PORT=<teamspeak server ip e.g. 77.666.555.4>:<teamspeak server port e.g. 12345>
export TS3_USER=<teamspeak bot user e.g. myBotUser>
export TS3_PASSWORD=<teamspeak bot password e.g. myBotPassword>
export DOCKER_HOST=ssh://<my_pi_ssh_alias>
docker login -u "<your_gitlab_user>" -p "<your_gitlab_user_pwd>" <your_gitlab_registry>
docker stack deploy -c docker-compose.yml --with-registry-auth ts3-bot
```

## Automatic Deployment
Create a another ssh key without password for the gitlab runner and copy the public key file again to your raspberry pi. Copy the contents of the private key and create in gitlab a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/) called `SSH_PRIVATE_KEY` with this content.

As above further custom gitlab CI/CD variables must be set:
```
PI_HOSTNAME=<ip or dyndns of your router e.g. my.ddns.address.net>
PI_PORT=<port which forwards incoming traffic from your router to your pi ssh port e.g. 32022>
TS3_IP_PORT=<teamspeak server ip e.g. 77.666.555.4>:<teamspeak server port e.g. 12345>
TS3_USER=<teamspeak bot user e.g. myBotUser>
TS3_PASSWORD=<teamspeak bot password e.g. myBotPassword>
```
