FROM python:3.6-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apk add --no-cache git openssh-client openssl ca-certificates linux-headers qemu-img libffi-dev openssl-dev build-base krb5 krb5-dev

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["python", "./main.py"]
