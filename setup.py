# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='sample',
    version='0.1.0',
    description='Sample package for Python-Guide.org',
    long_description=readme,
    author='Tobias Häuser',
    author_email='it.socke@gmail.com',
    url='https://gitlab.com/IT_Socke/ts3_bot',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
