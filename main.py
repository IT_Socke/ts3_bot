import ts3
import logging
import os
import sys

# Set normal logging
root        = logging.getLogger()
root.setLevel(logging.DEBUG)
formatter   = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# Set stdout logging
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
root.addHandler(handler)

# Config
try:
    ts3_ip_port = os.getenv('TS3_IP_PORT', '')
    ts3_user    = os.getenv('TS3_USER', '')
    ts3_passord = os.getenv('TS3_PASSWORD', '')
except Exception as ex:
    logging.warning(ex)

# Build URI
logging.info("ip and port:" + ts3_ip_port)
URI = "telnet://" + ts3_user + ":" + ts3_passord + "@" + ts3_ip_port

# Logging Settings
logging.basicConfig(level=logging.INFO)
logging.info("Bot Started")

# Connect to TeamSpeak Server
with ts3.query.TS3ServerConnection(URI) as ts3conn:
    ts3conn.exec_("use", port=6190)

    # Register for events
    ts3conn.exec_("servernotifyregister", event="server")

    while True:
        ts3conn.send_keepalive()

        try:
            event = ts3conn.wait_for_event(timeout=60)
        except ts3.query.TS3TimeoutError:
            logging.warning("Except pass")
            pass
        else:
            try:
                # Greet new clients.
                if event[0]["reasonid"] == "0":
                    logging.warning("client connected:" + str(event[0]["client_nickname"]))
                    ts3conn.exec_("clientpoke", clid=event[0]["clid"], msg="Hello :) Entwicklerstammtisch !!!")
            except Exception as ex:
                logging.warning(ex)
